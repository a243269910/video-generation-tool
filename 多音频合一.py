from pydub import AudioSegment  
  
def concatenate_mp3s(file_list, output_file):  
    """  
    拼接多个MP3文件。  
      
    :param file_list: 一个包含MP3文件路径的列表。  
    :param output_file: 输出文件的路径。  
    """  
    # 创建一个空的AudioSegment对象，用于累加音频  
    combined = AudioSegment.empty()  
  
    # 遍历MP3文件列表，将它们逐个加载并添加到combined中  
    for file in file_list:  
        audio_file = AudioSegment.from_mp3(file)  
        combined += audio_file  
  
    # 将合并后的音频保存到输出文件  
    combined.export(output_file, format="mp3")  
  
# 要拼接的MP3文件列表  
mp3_files =  ['jap/学生.mp3','jap/先生.mp3','jap/日本人.mp3']
  
# 输出文件的路径  
output_mp3 = "path_to_output_mp3.mp3"  
  
# 调用函数进行拼接  
concatenate_mp3s(mp3_files, output_mp3)  
  
print(f"MP3 files have been concatenated to {output_mp3}")