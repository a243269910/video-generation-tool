import re
import openai
import time
import json
from openai import OpenAI
from conf import GPT
from datetime import datetime, timedelta
def get_gpt_client():

    client = OpenAI(
        api_key= GPT.api,
        base_url=GPT.url,
    )
    return client

def chat(client,messages,model="gpt-4o-mini"):
    completion = client.chat.completions.create(
      model=model,
      messages=messages,

    )
    return completion.choices[0].message.content


import re
def read_json(file_path):
    with open(file_path, 'r', encoding='utf-8') as file:
        data = json.load(file)
    return data
def write_json(file_path, data):
    with open(file_path, 'w', encoding='utf-8') as file:
        json.dump(data, file, ensure_ascii=False, indent=4)


def parse_lrc_file(file_path):
    """
    Parse an LRC file and extract timestamps and lyrics without using regex.

    Args:
        file_path (str): Path to the LRC file

    Returns:
        list: List of tuples containing (timestamp, lyrics)
    """
    lyrics = []

    with open(file_path, 'r', encoding='utf-8') as file:
        for line in file:
            line = line.strip()

            # 跳过空行
            if not line:
                continue

            # 检查是否为歌词行（以 [ 开头）
            if line.startswith('[') and line.find(']') != -1:
                # 找到第一个 ] 的位置
                timestamp_end = line.find(']')

                # 提取时间戳（去掉中括号）
                timestamp = line[1:timestamp_end]

                # 提取歌词内容（去掉时间戳）
                lyric = line[timestamp_end + 1:].strip()

                # 只添加非空歌词
                if lyric:
                    lyrics.append((timestamp, lyric))
            else:
                # 非歌词行（可能是元数据）
                lyrics.append((None, line))

    return lyrics


def translate_lyric(lyric):
    """
    Translate a single lyric line from Japanese to Chinese using GPT.

    Args:
        lyric (str): Japanese lyric to translate

    Returns:
        str: Translated Chinese lyric
    """
    try:
        openai_cli = get_gpt_client()
        import json
        data = {"zh":'here is the chinese meaning',
                'phrase_split_list':"phrase1/phrase2/phrase3...",
                'phrase_split2zh':{'phrase1':'part_of_zh1','phrase2':'part_of_zh2'}}
        mess = [
                {"role": "system",
                 "content": "You are a professional translator specialized in translating Japanese song lyrics to Chinese, preserving the poetic and emotional nuance."},
                {"role": "user",
                 "content": f"Translate the following Japanese song lyric to Chinese, maintaining its original meaning and emotional tone ,and also do phrase splitting to the japanese lyric and find the corresponding chinese words : '{lyric}',the format of result should be json, like this:```json{json.dumps(data)}``` "}
            ]
        translated_lyric = chat(openai_cli,mess)
        translated_lyric=  translated_lyric.replace('```json','').replace('```','').replace('json','').strip()
        translated_lyric = translated_lyric.replace('\n','')
        return translated_lyric
    except Exception as e:
        print(f"Translation error for lyric '{lyric}': {e}")
        return lyric  # Return original lyric if translation fails


def translate_lrc_file(input_file, output_file):
    """
    Translate an entire LRC file from Japanese to Chinese.

    Args:
        input_file (str): Path to input LRC file
        output_file (str): Path to output translated LRC file
    """
    # Parse lyrics
    parsed_lyrics = parse_lrc_file(input_file)
    #assert len(parsed_lyrics) > 0
    # Translate lyrics with rate limiting
    translated_lyrics = []
    print('开始翻译')
    output_d= {
        'data':[]
    }
    for timestamp, lyric in parsed_lyrics[: ]:
        if '作词' in lyric or '作曲' in lyric:
            continue
        translated_lyric = translate_lyric(lyric)
        output_d['data'].append([timestamp,translated_lyric])
        #d = json.loads(translated_lyric)
        #translated_lyrics.append((timestamp, translated_lyric))
        #time.sleep(1)  # Rate limiting to avoid API restrictions

    write_json(output_file,output_d)


def split_sentence(sentence, words):
    """
    将句子按照给定词表分割，同时保持原始顺序和额外部分的分割

    Args:
        sentence (str): 需要分割的句子
        words (list): 用于分割的词表

    Returns:
        list: 分割后的词列表
    """
    # 记录每个word在sentence中的位置
    word_positions = []
    for word in words:
        pos = sentence.find(word)
        if pos != -1:
            word_positions.append((pos, pos + len(word), word))

    # 按照位置排序
    word_positions.sort()

    # 生成结果列表
    result = []
    last_end = 0

    # 遍历排序后的位置，处理每个区间
    for start, end, word in word_positions:
        # 如果当前word的起始位置大于上一个结束位置
        # 说明中间有未匹配的部分，需要添加到结果中
        if start > last_end:
            middle_part = sentence[last_end:start].strip()
            if middle_part:
                result.append(middle_part)

        # 添加当前word
        result.append(word)
        last_end = end

    # 处理最后一个word之后的部分
    if last_end < len(sentence):
        remaining = sentence[last_end:].strip()
        if remaining:
            result.append(remaining)
    print({"result":result,"words":words,'sentence':sentence})
    return result
def parse_timestamp(time_str):
    # 将时间字符串解析为datetime对象
    time_obj = datetime.strptime(time_str, '%M:%S.%f')
    # 计算总秒数
    total_seconds = timedelta(
        minutes=time_obj.minute,
        seconds=time_obj.second,
        microseconds=time_obj.microsecond
    ).total_seconds()

    return total_seconds
