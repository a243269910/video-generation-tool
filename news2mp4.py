from moviepy.editor import ImageClip,AudioFileClip,concatenate_videoclips,CompositeVideoClip,TextClip
import cv2
content = '''
近期

互联网企业之间是否应该屏蔽外链，如何实现互联互通

成为业内关注焦点。

9月13日，工业和信息化部部长肖亚庆

在国务院新闻办新闻发布会上对上述问题进行回应，

他表示，互联网安全是底线，

互联网的发展一定能够使得老百姓的生活更加方便，

助力和促进各方面的经济发展，最终促进互联网行业平台经济健康有序地发展。

“如何保障合法合规的网址正常访问，是互联网发展基本需求。

无正当理由限制识别、解析、正常访问，严重影响用户体验，

也扰乱了市场秩序，投诉举报很多”，

工业和信息化部信息通信管理局局长赵志国补充道。

他表示，最终希望推动形成，互通开放、规范有序、保证安全的互联网发展环境。

赵志国称，工信部正在按照专项行动方案安排，指导互联网企业开展自查整改。

在自查整改中了解到，部分企业对屏蔽网址的认识与专项行动要求有一定差距，

我们采取行政指导会等多种形式帮助企业认识到，

互联互通是互联网企业高质量发展的必然选择，

让用户畅通安全地使用互联网也是互联网行业必须努力的方向。

赵志国还要求企业按照整改要求务实推动即时通信、屏蔽网址链接等不同类型的问题，

分步骤、分阶段得到解决。

下一步，一是加强行政指导，对整改不到位的企业督促落实；

二是加强监督检查，通过多种方式确保问题整改到位；

三是对整改不彻底的企业，处罚一批典型违规企业。

会后，腾讯对工信部决策进行了官方回应：我们坚决拥护工信部的决策，在以安全为底线的前提下，分阶段分步骤地实施。

'''
subtitles = content.split()
frames = len(subtitles)
#print(content.split())
res = []
text_clip = []
fontsize =30
white = (255,255,255)
audios = []
#subtitles = ['line1','line2','line3']
font_path = "/System/Library/Fonts/Supplemental/Arial Unicode.ttf"
from voicedemo import *
from PIL import ImageFont, ImageDraw, Image
import numpy as np
w ,h = 400 ,200
for ix,text  in enumerate(subtitles):
    fs = 32
    # font = ImageFont.truetype(font_path, fs)
    # bk_img = np.array([255]* w *h*3).reshape((h,w,3))
    # img_pil = Image.fromarray(np.uint8(bk_img))
    # draw = ImageDraw.Draw(img_pil)
    # # 绘制文字信息
    # # subtitle text
    # # position from top left (x,y)
    # white = (255, 255, 255)
    # red = (255, 0, 0)
    # draw.text((0, 100), subtitles[ix], font=font, fill=red)

    # bk_img = np.array(img_pil)
    # # cv2.imshow("add_text", bk_img)d
    # # cv2.waitKey()
    # cv2.imwrite("add_text.jpg", bk_img)

    v1 = ImageClip('newbg.jpeg')
    #v1 = ImageClip("add_text.jpg")
    # 旁白
    audioSaveFile ='news{}.wav'.format(ix)
    processGETRequest(appKey, token, get_url_encode(text), audioSaveFile, format, sampleRate)
    a = AudioFileClip('news{}.wav'.format(ix))
    v1 = v1.set_duration(a.duration)
    v1 = v1.set_audio(a)
    res.append(v1)
    s = sum([a.duration for a in audios])
    print('start',s)
    # 字幕
    txt_clip1 = (
        TextClip(subtitles[ix], font=font_path, fontsize=fontsize, color='white', method='label')
            .set_position(("left", "bottom"))
            .set_duration(a.duration)
            .set_start(s)
    )
    audios.append(a)
    text_clip.append(txt_clip1)
result =concatenate_videoclips(res)
clip = CompositeVideoClip([result]+text_clip)
clip.write_videofile('result2.mp4',audio_codec='aac',fps=10)
