import subprocess
import os
def text2voice(text_for_play_,filename):
    """
    文本合成声音
    :param text_for_play_: 文本
    :param filename: 输出文件名 ， mp3格式
    :return:
    """
    command = f'edge-tts --voice zh-CN-XiaoyiNeural --text "{text_for_play_}" --write-media {filename}'  # 将 AI 生成的文本传递给 edge-tts 命令
    subprocess.run(command, shell=True)  # 执行命令行指令

def make_rly_voice(text_for_play_,ix):
    command = f'edge-tts --voice zh-CN-XiaoyiNeural --text "{text_for_play_}" --write-media room_mp3/output{ix}.mp3'  # 将 AI 生成的文本传递给 edge-tts 命令
    subprocess.run(command, shell=True)  # 执行命令行指令
    return f'room_mp3/output{ix}.mp3'

def make_realtime_rly_voice(text_for_play_,ix):
    if 'txt' in text_for_play_:
        command = f'edge-tts --voice zh-CN-XiaoyiNeural -f "{text_for_play_}" --write-media room_mp3/output{ix}.mp3'  # 将 AI 生成的文本传递给 edge-tts 命令
        os.system(command)  # 执行命令行指令
        return f'room_mp3/output{ix}.mp3'

    command = f'edge-tts --voice zh-CN-XiaoyiNeural --text "{text_for_play_}" --write-media room_mp3/output{ix}.mp3'  # 将 AI 生成的文本传递给 edge-tts 命令
    os.system(command)  # 执行命令行指令
    return f'room_mp3/output{ix}.mp3'

def play_real_time_voice(ix):
    command = f'mpv.exe -vo null room_mp3/output{ix}.mp3'  # 播放音频文件
    os.system(command )  # 执行命令行指令

def delete_voice(ix):
    if os.path.exists(f'room_mp3/output{ix}.mp3'):
        os.remove(f'room_mp3/output{ix}.mp3')
def play_voice(ix):
    command = f'mpv.exe -vo null room_mp3/output{ix}.mp3'  # 播放音频文件
    subprocess.run(command, shell=True)  # 执行命令行指令
