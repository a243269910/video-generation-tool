import os
import cv2
import moviepy.video.io.ImageSequenceClip
from moviepy.editor import AudioFileClip,CompositeVideoClip,TextClip
def make_mp4(img_sources, output,subtitle,fps =3,fontsize = 25,interval = 1,bg_path = 'syAudio1.wav',font_path="/System/Library/Fonts/Supplemental/Arial Unicode.ttf",size=(480, 480)):
    clip = moviepy.video.io.ImageSequenceClip.ImageSequenceClip(img_sources,fps)
    audio = AudioFileClip(bg_path)
    clip = clip.set_audio(audio)
    #subtitle = subtitle.split('，')
    text_clip = []
    for ix,line in enumerate(subtitle):
        txt_clip1 = (
            TextClip(line, font=font_path, fontsize=fontsize, color='white', method='label')
                .set_position(("center", "bottom"))
                .set_duration(interval)
                .set_start(ix*interval)
        )
        text_clip.append(txt_clip1)
    clip = CompositeVideoClip([clip]+text_clip, size = size )
    clip.write_videofile(output,audio_codec="aac")

def is_valid_imgs(img_sources):
    sizes  = []
    for imgfile in img_sources:
        img = cv2.imread(imgfile)
        sizes.append(img.size())
    if  len(set(sizes)) ==1:
        return True
    return False